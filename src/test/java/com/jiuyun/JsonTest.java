package com.jiuyun;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jiuyun.dao.ClassesDao;
import com.jiuyun.dao.StudentDao;
import com.jiuyun.dao.imol.ClassesDaoImpl;
import com.jiuyun.dao.imol.StudentDaoImpl;
import com.jiuyun.entiy.Student;
import com.jiuyun.vo.ChartData;
import com.jiuyun.vo.RadarData;
import org.junit.Test;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class JsonTest {
    StudentDao dao=new StudentDaoImpl();
//    Gson gson=new Gson();
    Gson gson = new GsonBuilder().disableHtmlEscaping().create();
    @Test
    public void test1() throws SQLException {
        List list=dao.getUsersr();
        System.out.println(list);
        String t=gson.toJson(list);
        System.out.println(t);
    }

    @Test
    public void test2(){
        String s="{\"userName\":\"张三\",\"passWord\":\"12345\",\"sex\":\"男\"}";
        Student stu= gson.fromJson(s, Student.class);
        System.out.println(s);
        System.out.println(stu);
    }

    @Test
    public void text4() throws SQLException {
        ClassesDao dao= new ClassesDaoImpl();
        List list=dao.getClasses();
        String s=gson.toJson(list);
        System.out.println(s);
    }
}
