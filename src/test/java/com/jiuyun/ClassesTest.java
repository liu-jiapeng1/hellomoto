package com.jiuyun;

import com.jiuyun.dao.ClassesDao;
import com.jiuyun.dao.imol.ClassesDaoImpl;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

public class ClassesTest {
    //1班级
    ClassesDao dao=new ClassesDaoImpl();
    @Test
    public void test1() throws SQLException{
        List list=dao.getClasses();
        System.out.println(list);
    }
    @Test
    public void test2() throws SQLException{
        List list=dao.getStudents(1);
        System.out.println(list);
    }
    @Test
    public void test3() throws SQLException{
        List list=dao.getHomework(4,2021,1);
        System.out.println(list);
    }

    private class var {
    }
}
