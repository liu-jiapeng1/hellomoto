package com.jiuyun;

import com.jiuyun.dao.AddDao;
import com.jiuyun.dao.StudentDao;
import com.jiuyun.dao.imol.AddDaoImpl;
import com.jiuyun.entiy.Student;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

public class ExcelTest {

    public static void main(String[] args) throws IOException, SQLException {
        ClassLoader classLoader=ExcelTest.class.getClassLoader();
        //读取文件
        String filename="hello.xlsx";
        InputStream input=classLoader.getResourceAsStream(filename);
        XSSFWorkbook sheets=new XSSFWorkbook(input);
        XSSFSheet sheet=sheets.getSheetAt(0);
        //读取行数
        int rows=sheet.getPhysicalNumberOfRows();
        //读取列
        XSSFRow row=sheet.getRow(0);
        int cols=row.getPhysicalNumberOfCells();
        System.out.println(rows+""+cols);
        for (int i=1;i<rows;i++){
            row=sheet.getRow(i);
            String c1=row.getCell(0).toString();
            String c2=row.getCell(1).toString();
            String c3=row.getCell(2).toString();
            int c4=(int)(row.getCell(3).getNumericCellValue());
            int c5=(int)(row.getCell(4).getNumericCellValue());
            String c6=row.getCell(5).toString();
            int c7=(int)(row.getCell(6).getNumericCellValue());
            System.out.println(c1+"--"+c2+"--"+c3+"--"+c4+"--"+c5+"--"+c6+"--"+c7);
            AddDao st=new AddDaoImpl();
            Student student=new Student();
            student.setUserName(c1);
            student.setPassWord(c2);
            student.setSex(c3);
            student.setAge(c4);
            student.setDianhua(c5);
            student.setAddress(c6);
            student.setClassId(c7);
            int ok= st.add(student);
            System.out.println(ok);
        }
        input.close();
    }
    @Test
    public void ttAdd() throws SQLException {
        AddDao st=new AddDaoImpl();
        Student student=new Student();
        student.setUserName("德玛");
        student.setPassWord("123456");
        student.setSex("男");
        student.setAge(18);
        student.setDianhua(145432);
        student.setAddress("江西");
        student.setClassId(6);
        int ok= st.add(student);
        System.out.println(ok);
    }
}
