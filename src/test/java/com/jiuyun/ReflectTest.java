package com.jiuyun;

import com.jiuyun.entiy.Student;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 反射的测试类
 * */
public class ReflectTest {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
          Class c=Student.class;//类名
//          Class c=new Student().getClass();//对象名
//          Class c=Class.forName("com.jiuyun.entiy.Student");
//        //测试所有属性
//        Field sx[]=c.getDeclaredFields();
//        for (Field s:sx){
//            System.out.println(s.getName());
//        }
//        //所有自定义的方法
//        Method ff[]=c.getDeclaredMethods();
//        //获取父类继承的方法
////        Method ff[]=c.getMethods();
//        for (Method f:ff){
//            System.out.println("方法"+f.getName());
//        }
        //获取其中一个(方法名,参数列表...)
        Method m=c.getDeclaredMethod("setStudentId", int.class);
        System.out.println("一个"+m.getName());

//        //调用方法 --原来 对象.方法()
//        Student st=new Student();
//        st.setUserName("打野");
        //反射调用 m
        Object obj=c.newInstance();
        m.invoke(obj,1);
        System.out.println(obj);
    }
}
