package com.jiuyun.entiy;

public class Auth {
    int authid;
    String authName;
    String authPath;

    @Override
    public String toString() {
        return "Auth{" +
                "authId=" + authid +
                ", authName='" + authName + '\'' +
                ", authPath='" + authPath + '\'' +
                '}';
    }

    public int getAuthId() {
        return authid;
    }

    public void setAuthId(int authId) {
        this.authid = authId;
    }

    public String getAuthName() {
        return authName;
    }

    public void setAuthName(String authName) {
        this.authName = authName;
    }

    public String getAuthPath() {
        return authPath;
    }

    public void setAuthPath(String authPath) {
        this.authPath = authPath;
    }
}
