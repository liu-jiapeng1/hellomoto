package com.jiuyun.entiy;

public class Student {

    String userName;
    String passWord;
    String sex,address;
    int studentId,classId,age,dianhua;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getDianhua() {
        return dianhua;
    }

    public void setDianhua(int dianhua) {
        this.dianhua = dianhua;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "Student{" +
                "userName='" + userName + '\'' +
                ", passWord='" + passWord + '\'' +
                ", sex='" + sex + '\'' +
                ", address='" + address + '\'' +
                ", studentId=" + studentId +
                ", classId=" + classId +
                ", age=" + age +
                ", dianhua=" + dianhua +
                '}';
    }
}
