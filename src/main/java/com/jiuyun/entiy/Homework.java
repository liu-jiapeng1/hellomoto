package com.jiuyun.entiy;

import java.util.Date;

/***
 * 作业情况
 * */
public class Homework {
    private int id;
    private int studentId;
    private int work;//作业
    private Date ctime;//那一天
    private String remark;//原因
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public int getWork() {
        return work;
    }

    public void setWork(int work) {
        this.work = work;
    }

    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "\nHomework{" +
                "id=" + id +
                ", studentId=" + studentId +
                ", work=" + work +
                ", ctime=" + ctime +
                ", remark='" + remark + '\'' +
                ", userName='" + userName + '\'' +
                '}';
    }
}
