package com.jiuyun.servlet;

import com.jiuyun.servlet.BaseServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/user/*")
public class HelloServlet extends BaseServlet {

    protected void cart(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("运行 购物车查询()");
    }

    protected void order(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("运行 订单查询()");
    }

    protected void info(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("运行 个人信息()");
    }
}
