package com.jiuyun.servlet;

import com.google.gson.Gson;
import com.jiuyun.vo.ChartData;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/echart")
public class EchartServlet extends HttpServlet {
        Gson gson=new Gson();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //本应该来自数据库
        String title[]={"丝袜","帽子","裙子","衬衫","羊毛衫","雪纺衫"};
        int data[]={10,17,31,22,11,18};
        ChartData cd=new ChartData(title,data);
        String s=gson.toJson(cd);
        //如何让servlet返回JSON数据
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("application/json");
        PrintWriter out=resp.getWriter();
        out.print(s);//数据
        out.flush();
        out.close();

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }
}
