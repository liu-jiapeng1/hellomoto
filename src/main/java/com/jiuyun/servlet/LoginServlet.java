package com.jiuyun.servlet;
import com.jiuyun.dao.AuthDao;
import com.jiuyun.dao.imol.AuthDaoImpl;
import com.jiuyun.entiy.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

// 完成登录
@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    AuthDao authDao = new AuthDaoImpl();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("stname");
        String pass = request.getParameter("stpass");
        User user =null;
        try {
            user = authDao.login(name,pass);
            if(user == null){
                System.out.println("登录失败");
            }else{
                //成功 session放个标志
                HttpSession session =  request.getSession();
                List list = authDao.myauth( user.getUid() );
                session.setAttribute("LOGIN",user);
                session.setAttribute("auths",list);
                response.sendRedirect("/CG/index.jsp");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request,response);
    }
}
