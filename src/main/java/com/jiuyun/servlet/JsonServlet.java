package com.jiuyun.servlet;

import com.google.gson.Gson;
import com.jiuyun.dao.StudentDao;
import com.jiuyun.dao.imol.StudentDaoImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.Response;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;


@WebServlet("/json")
public class JsonServlet extends HttpServlet {
    StudentDao dao=new StudentDaoImpl();
    Gson gson=new Gson();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String s=null;
        try {
            List list=dao.getUsersr();
            s=gson.toJson(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //如何让servlet返回JSON数据
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("application/json");
        PrintWriter out=resp.getWriter();
        out.print(s);//数据
        out.flush();
        out.close();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }
}
