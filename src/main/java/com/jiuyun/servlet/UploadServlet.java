package com.jiuyun.servlet;

import com.jiuyun.dao.AddDao;
import com.jiuyun.dao.imol.AddDaoImpl;
import com.jiuyun.entiy.Student;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadBase;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.xmlbeans.impl.xb.xsdschema.impl.ListDocumentImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/upload")
public class UploadServlet extends HttpServlet {
    final int MAX_FILE_SIZE=1024*1024*4;//4M
    String fname="";
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //文件上传后的位置 服务器的运行位置
        String appPath =req.getServletContext().getRealPath("WEB-INF/classes");
        System.out.println("运行位置"+appPath);
        DiskFileItemFactory factory=new DiskFileItemFactory();
        ServletFileUpload upload=new ServletFileUpload(factory);
        //设置最大文件上传值 默认2M
        upload.setFileSizeMax(MAX_FILE_SIZE);
        upload.setHeaderEncoding("utf-8");
        //解析请求内容提取文件数据try {
        try {
            List<FileItem> formItems=upload.parseRequest(req);
            for (FileItem item : formItems){
                if (item.isFormField()){
                    //???
                }else {
                    //文件
                    fname= item.getName();
                    File file=new File(appPath+"/"+fname);
                    item.write(file);//存盘
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        ClassLoader classLoader=UploadServlet.class.getClassLoader();
        //读取文件
        String filename=fname;
        InputStream input=classLoader.getResourceAsStream(filename);
        XSSFWorkbook sheets=new XSSFWorkbook(input);
        XSSFSheet sheet=sheets.getSheetAt(0);
        //读取行数
        int rows=sheet.getPhysicalNumberOfRows();
        //读取列
        XSSFRow row=sheet.getRow(0);
        int cols=row.getPhysicalNumberOfCells();
        System.out.println(rows+""+cols);
        for (int i=1;i<rows;i++){
            row=sheet.getRow(i);
            String c1=row.getCell(0).toString();
            String c2=row.getCell(1).toString();
            String c3=row.getCell(2).toString();
            int c4=(int)(row.getCell(3).getNumericCellValue());
            int c5=(int)(row.getCell(4).getNumericCellValue());
            String c6=row.getCell(5).toString();
            int c7=(int)(row.getCell(6).getNumericCellValue());
            System.out.println(c1+"--"+c2+"--"+c3+"--"+c4+"--"+c5+"--"+c6+"--"+c7);
            AddDao st=new AddDaoImpl();
            Student student=new Student();
            student.setUserName(c1);
            student.setPassWord(c2);
            student.setSex(c3);
            student.setAge(c4);
            student.setDianhua(c5);
            student.setAddress(c6);
            student.setClassId(c7);
            int ok= 0;
            try {
                ok = st.add(student);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            System.out.println(ok);
        }
        input.close();
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req, resp);
    }
}
