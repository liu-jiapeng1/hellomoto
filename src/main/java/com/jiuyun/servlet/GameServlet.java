package com.jiuyun.servlet;

import com.jiuyun.servlet.BaseServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

// 没登录不让访问

@WebServlet("/vip/*")
public class GameServlet extends BaseServlet {

    protected void tv(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("运行 收费视频()");
    }

    protected void adv(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("运行 免广告()");
    }

    protected void gift(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("运行 礼物()");
    }
}
