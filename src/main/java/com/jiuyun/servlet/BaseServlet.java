package com.jiuyun.servlet;

import com.mysql.cj.x.protobuf.MysqlxConnection;
import com.mysql.cj.x.protobuf.MysqlxSession;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;

public class BaseServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String url=req.getRequestURI();
        int gang=url.lastIndexOf("/");
        String mname=url.substring(gang+1);
        System.out.println("运行了service()--->"+mname);
        Class c=this.getClass();
        try {
            Method m=c.getDeclaredMethod(mname,HttpServletRequest.class, HttpServletResponse.class);
                m.invoke(this,req,resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
