package com.jiuyun.servlet;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jiuyun.vo.ChartData;
import com.jiuyun.vo.RadarData;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet ("/radar")
public class RadarServlet extends HttpServlet {
    Gson gson = new GsonBuilder().disableHtmlEscaping().create();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //本应该来自数据库
        String s1="[{ name: '身份特质', max: 6500}," +
                "{ name: '行为偏好', max: 16000}," +
                "{ name: '人脉关系', max: 30000}," +
                "{ name: '信用历史', max: 38000}," +
                "{ name: '履约能力', max: 52000}]";
        int data[]={4200, 13000, 20000, 35000, 38000};
        String indicator=gson.toJson(s1);
        RadarData cd=new RadarData(indicator,data);
        String s=gson.toJson(data);
        //如何让servlet返回JSON数据
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("application/json");
        PrintWriter out=resp.getWriter();
        out.print(s);//数据
        out.flush();
        out.close();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }
}
