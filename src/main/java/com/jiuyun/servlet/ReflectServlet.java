package com.jiuyun.servlet;

import com.google.gson.Gson;
import com.jiuyun.dao.AddDao;
import com.jiuyun.dao.ClassesDao;
import com.jiuyun.dao.imol.AddDaoImpl;
import com.jiuyun.dao.imol.ClassesDaoImpl;
import com.jiuyun.entiy.Homework;
import com.jiuyun.service.ClassService;
import com.jiuyun.service.imol.ClassServiceImpl;
import com.sun.corba.se.spi.orbutil.threadpool.Work;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@WebServlet("/reflect/*")
public class ReflectServlet extends BaseServlet{
    Gson gson=new Gson();
    ClassService service=new ClassServiceImpl();
    ClassesDao dao=new ClassesDaoImpl();
    AddDao dao1=new AddDaoImpl();
    //学生信息
    protected void xue(HttpServletRequest req, HttpServletResponse resp) throws IOException, SQLException {
        //班级ID
        String cid=req.getParameter("cid");
        List stus=dao.getStudents(Integer.parseInt(cid));
        String s=gson.toJson(stus);
        //如何让servlet返回JSON数据
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("application/json");
        PrintWriter out=resp.getWriter();
        out.print(s);//数据
        out.flush();
        out.close();
    }
    //班级信息 json
    protected void ban(HttpServletRequest req, HttpServletResponse resp) throws SQLException, IOException {
        List list=dao.getClasses();
        String s=gson.toJson(list);
        //如何让servlet返回JSON数据
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("application/json");
        PrintWriter out=resp.getWriter();
        out.print(s);//数据
        out.flush();
        out.close();
    }
    //查询作业信息
    protected void search(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HashMap<String,int[]> info= null;
        String banid=req.getParameter("cid");
        String riqi=req.getParameter("riqi");
        int cid=Integer.parseInt(banid);
        String sz[]=riqi.split("-");
        int year=Integer.parseInt(sz[0]);
        int m=Integer.parseInt(sz[1]);
        try {
            info = service.homework(cid,year,m);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        req.setAttribute("info",info);

        req.getRequestDispatcher("/classes.jsp").forward(req,resp);
    }
    //增加作业信息
    protected void add(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, ParseException, SQLException {
        Homework work=new Homework();
        String sid=req.getParameter("sid");
        String remark=req.getParameter("remark");
        String ctime=req.getParameter("ctime");
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        work.setStudentId(Integer.parseInt(sid));
        work.setCtime(sdf.parse(ctime));
        work.setRemark(remark);
        //1=对了  0=错误
        int n = dao1.getAdd(work);
        //如何让servlet返回JSON数据
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("application/json");
        PrintWriter out=resp.getWriter();
        out.print("{\"code\":"+n+"}");//数据
        out.flush();
        out.close();
    }
}
