package com.jiuyun.servlet;

import com.jiuyun.dao.ClassesDao;
import com.jiuyun.dao.imol.ClassesDaoImpl;
import com.jiuyun.service.ClassService;
import com.jiuyun.service.imol.ClassServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

@WebServlet("/homework")
public class HomeworkServlet extends HttpServlet {
    ClassService service=new ClassServiceImpl();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HashMap<String,int[]> info= null;
        try {
            info = service.homework(4,2021,1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
            req.setAttribute("info",info);
            req.getRequestDispatcher("classes.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }
}