package com.jiuyun.service.imol;

import com.jiuyun.dao.ClassesDao;
import com.jiuyun.dao.imol.ClassesDaoImpl;
import com.jiuyun.entiy.Homework;
import com.jiuyun.entiy.Student;
import com.jiuyun.service.ClassService;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public class ClassServiceImpl implements ClassService {
    ClassesDao classDao=new ClassesDaoImpl();
    @Override
    public HashMap<String, int[]> homework(int classId,int year,int month) throws SQLException {
        HashMap<String, int[]> info=new HashMap<String, int[]>();
        int days[]={30,28,31,30,31,30,31,31,30,31,30,31};
        if (year%4==0&&year%100!=0||year%400==0){
            days[1]=29;//闰年29天
        }
        int num=days[month-1];//本月天数
        //所有学生
        List<Student> stus=classDao.getStudents(classId);
        //没做作业的列表
        List<Homework> works=classDao.getHomework(classId,year,month);
        for (Student s:stus){
            info.put(s.getUserName(),new int[num]);
        }
        for (Homework h:works){
            String stname=h.getUserName();
            int t=h.getCtime().getDate();//天
            int rq[] =info.get(stname);
            rq[t-1]=1;//没做的标志为1
        }
        return info;
    }
}
