package com.jiuyun.service;

import java.sql.SQLException;
import java.util.HashMap;

/**
 * 完成班级作业完成
 * */
public interface ClassService {
    public HashMap<String,int[]> homework(int classId,int year,int month) throws SQLException;
}
