package com.jiuyun.dao;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public interface ClassesDao {
    //获取所有班级
    public List getClasses() throws SQLException;
    //获取某班所有学生
    public List getStudents(int classId) throws SQLException;
    //获取某月、某班作业情况
    public List getHomework(int classId,int year,int month) throws  SQLException;
}
