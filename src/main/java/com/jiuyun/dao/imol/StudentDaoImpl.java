package com.jiuyun.dao.imol;

import com.jiuyun.dao.StudentDao;
import com.jiuyun.entiy.Student;
import com.jiuyun.utils.AliPool;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StudentDaoImpl implements StudentDao {
    @Override
    public List getUsersr() throws SQLException {
        String sql="select * from user";
        Connection conn= AliPool.getConn();
        PreparedStatement pt= conn.prepareCall(sql);
        ResultSet rs=pt.executeQuery();
        List list=new ArrayList();
        while (rs.next()){
            Student st=new Student();
            st.setUserName(rs.getString(2));
            st.setPassWord(rs.getString(3));
            st.setSex(rs.getString(4));
            list.add(st);//放入集合
        }
        conn.close();
        return list;
    }

}
