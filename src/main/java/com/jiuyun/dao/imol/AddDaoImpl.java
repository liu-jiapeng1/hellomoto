package com.jiuyun.dao.imol;

import com.jiuyun.dao.AddDao;
import com.jiuyun.entiy.Homework;
import com.jiuyun.entiy.Student;
import com.jiuyun.utils.AliPool;
import org.apache.commons.dbutils.QueryRunner;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

public class AddDaoImpl implements AddDao {
    QueryRunner runner= new QueryRunner();
    @Override
    public int getAdd(Homework work) throws SQLException {
        String sql="insert into homework values (null ,?,1,?,?)";
        Connection conn= AliPool.getConn();
        int n= runner.update(conn,sql,work.getStudentId(),work.getCtime(),work.getRemark());
        conn.close();
        return n ;
    }
    @Override
    public int add(Student st) throws SQLException {
        String sql="insert into user(userName,passWord,sex,age,dianhua,address,classId) values(?,?,?,?,?,?,?)";
        Connection conn=AliPool.getConn();
        int i=runner.update(conn,sql,st.getUserName(),st.getPassWord(),st.getSex(),st.getAge(),st.getDianhua(),st.getAddress(),st.getClassId());
        conn.close();
        return i;
    }
}
