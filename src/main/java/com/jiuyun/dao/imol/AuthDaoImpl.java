package com.jiuyun.dao.imol;

import com.jiuyun.dao.AuthDao;
import com.jiuyun.entiy.Auth;
import com.jiuyun.entiy.User;
import com.jiuyun.utils.AliPool;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class AuthDaoImpl implements AuthDao {
    QueryRunner runner=new QueryRunner();
    @Override
    public User login(String name, String pass) throws SQLException {
        String sql="select * from user where uname=? and upass=?";
        Connection conn= AliPool.getConn();
        User user=runner.query(conn,sql,new BeanHandler<>(User.class),name,pass);
        conn.close();
        return user;
    }

    @Override
    public List<Auth> myauth(int uid) throws SQLException {
        String sql="select * from myauth where uid=?";
        Connection conn= AliPool.getConn();
        List list=runner.query(conn,sql,new BeanListHandler<Auth>(Auth.class),uid);
        conn.close();
        return list;
    }
}
