package com.jiuyun.dao;

import com.jiuyun.entiy.Auth;
import com.jiuyun.entiy.User;

import java.sql.SQLException;
import java.util.List;

public interface AuthDao {
    //登录
    public User login(String name,String pass) throws SQLException;
    //用户ID 查某人的权限
    public List<Auth> myauth(int uid) throws SQLException;

}
