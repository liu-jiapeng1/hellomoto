package com.jiuyun.dao;

import com.jiuyun.entiy.Homework;
import com.jiuyun.entiy.Student;

import java.sql.SQLException;
import java.util.Date;

public interface AddDao {
    //增加作业情况
    public int getAdd(Homework work) throws SQLException;
    public int add(Student st) throws SQLException;
}
