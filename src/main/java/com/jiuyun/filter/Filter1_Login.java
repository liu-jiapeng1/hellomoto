package com.jiuyun.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("*.do")
public class Filter1_Login implements Filter {

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        System.out.println("登录 正在过滤 。。。");
        HttpServletRequest request= (HttpServletRequest)req;
        HttpSession session = request.getSession();
        Object key = session.getAttribute("LOGIN");
        if(key == null){
            System.out.println("非法用户");
        }else{
            //放行
            chain.doFilter(req, resp);
        }
    }

    public void init(FilterConfig config) throws ServletException {
        System.out.println("hello 初始化");
    }

    public void destroy() {
        System.out.println("hello 销毁");
    }


}
