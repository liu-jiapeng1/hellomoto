package com.jiuyun.vo;

import java.util.List;

public class RadarData {
    String indicator;
    int data[];

    public RadarData(String indicator, int[] data) {
        this.indicator = indicator;
        this.data = data;
    }

    public String getIndicator() {
        return indicator;
    }

    public void setIndicator(String indicator) {
        this.indicator = indicator;
    }

    public int[] getData() {
        return data;
    }

    public void setData(int[] data) {
        this.data = data;
    }
}
