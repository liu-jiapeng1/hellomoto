<%@ page import="java.util.Date" %><%--
  Created by IntelliJ IDEA.
  User: 16063
  Date: 2021/8/9
  Time: 14:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Title</title>
    <%
        request.setAttribute("now",new Date());
        request.setAttribute("money",6000);
    %>
</head>
<body>
    <h1>JSTL 日期格式</h1>
    <br>${now}
    <br><fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm:ss E" />
    <h1>Iphone 价格</h1>
    <br>${money}
    <br><fmt:formatNumber type="currency" value="${money}" />
</body>
</html>
