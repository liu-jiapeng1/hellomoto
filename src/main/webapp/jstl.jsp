<%@ page import="com.jiuyun.dao.ClassesDao" %>
<%@ page import="com.jiuyun.dao.imol.ClassesDaoImpl" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: 16063
  Date: 2021/8/4
  Time: 15:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <%
        request.setAttribute("num",8);
        request.setAttribute("money",20000);
        request.setAttribute("week",3);
        String names[]={"提莫","小丑","人马","螳螂","EZ"};
        request.setAttribute("heros",names);

        ClassesDao dao=new ClassesDaoImpl();
        List clist=dao.getClasses();
        request.setAttribute("clist",clist);
    %>
</head>
<body>
    <h1>JSTL 测试</h1>
    <h3>数据=${num}</h3>
    <h3>数据=<c:out value="${num}" default="没找到" escapeXml="false" /></h3>
    <h3>IF</h3>
    <c:if test="${money>=5000}">
        你的工资${money}可以找到老婆
    </c:if>
    <c:if test="${money>5000}">
        还可以玩游戏
    </c:if>

    <h3>choose,when,otherwise</h3>
    <c:choose>
        <c:when test="${work==1}">
            我去看电影
        </c:when>
        <c:when test="${work==2}">
            我去排位
        </c:when>
        <c:otherwise>
            搞学习
        </c:otherwise>
    </c:choose>

    <h3>forEach</h3>
    <c:forEach begin="1" end="${num}" var="k">
       <a href="#">${k}</a>
    </c:forEach>
    <c:forEach items="${heros}" var="obj" varStatus="i">
        <br><a>${i.index}---${obj}---${i.count}</a>
    </c:forEach>

    <%--集合+对象 HashMap+对象--%>
    <c:forEach items="${clist}" var="ban">
        <br>${ban.classId}--${ban.className}
    </c:forEach>
</body>
</html>
