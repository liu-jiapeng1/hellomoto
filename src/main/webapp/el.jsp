<%@ page import="com.jiuyun.entiy.Student" %>
<%@ page import="com.mysql.cj.Session" %><%--
  Created by IntelliJ IDEA.
  User: 16063
  Date: 2021/8/4
  Time: 14:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

    <%
        //模拟servlet 传来的数据
        request.setAttribute("height",50);
        request.setAttribute("handsome",90);
        //obj
        Student temo=new Student();
        temo.setClassId(90);
        temo.setUserName("提莫");
        temo.setPassWord("123456");
        session.setAttribute("temo",temo);
    %>
    <h1>EL 表达式</h1>
    <h3>取值=${height}</h3>
    <h3>逻辑运行=${height>50 and handsome>80}</h3>
    <h3>逻辑运行=${height>50 or handsome>80}</h3>
    <h3>空${empty handsome}</h3>
    <h3>对象属性 姓名:${temo.userName} 密码:${temo['passWord']}</h3>
</body>
</html>
