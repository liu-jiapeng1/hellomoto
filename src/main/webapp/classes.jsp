<%@ page import="java.util.Arrays" %><%--
  Created by IntelliJ IDEA.
  User: 16063
  Date: 2021/7/19
  Time: 16:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <base href="http://localhost:8081/CG/"/>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>测试 - layui</title>
    <link rel="stylesheet" href="layui/css/layui.css">
    <style>
        .center{ text-align: center;}
        .ftd{ width:45px;display:inline-block}
        .myh1{margin:50px 0;text-align: center;}
    </style>
</head>
<body>
<h1 class="center">作业完成情况</h1>
<div class="layui-container myh1">
    <table class="layui-table">
        <thead>
            <tr>
                <th class="ftd">姓名</th>
                <c:forEach begin="1" end="31" var="d">
                        <th>${d}</th>
                </c:forEach>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${info}" var="h">
                <tr>
                <th>${h.key}</th>
                    <c:forEach items="${h.value}" var="k">
                        <c:if test="${k==1}">
                            <td><i  class="layui-icon layui-icon-face-cry" style="font-size: 25px; color: red"></i></td>
                        </c:if>
                        <c:if test="${k==0}">
                            <td><i class="layui-icon layui-icon-face-smile" style="font-size: 25px; color: green"></i></td>
                        </c:if>
                    </c:forEach>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>
</body>
</html>
