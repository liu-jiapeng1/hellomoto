<%--
  Created by IntelliJ IDEA.
  User: sanzang
  Date: 2021/8/13
  Time: 3:00 下午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>权限测试  主页</h1>

<h3>普通</h3>
<a href="user/cart">购物车</a>
<a href="user/order">订单</a>
<a href="user/info">个人信息</a>

<h3>会员</h3>
<a href="vip/tv">收费视频</a>
<a href="vip/adv">免广告</a>
<a href="vip/gift">礼物</a>

</body>
</html>
